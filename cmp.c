#include <stdbool.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <time.h> 

// 8 bytes
#define BUFF_LEN 8 // #bytes

void print_diff_16_bytes(FILE *fp1, FILE *fp2, int pos)
{
    unsigned char *diff_buff1, *diff_buff2;
    diff_buff1 = malloc(sizeof(unsigned char)*1);
    diff_buff2 = malloc(sizeof(unsigned char)*1);

    fseek ( fp1, pos, SEEK_SET);
    fseek ( fp2, pos, SEEK_SET);
    printf("\nDIFF\n");
    int cnt1 = fread(diff_buff1, sizeof(diff_buff1), 1,fp1);
    int cnt2 = fread(diff_buff2, sizeof(diff_buff2), 1,fp2);
    for(int i=1;i<16&&(cnt1||cnt2);i++)
    {
        if(cnt1)
            printf("File1 : 0x%x\t", *diff_buff1);
        else
            printf("File1 : EOF\t");
        if(cnt2)
            printf("File2 : 0x%x\n", *diff_buff2);
        else
            printf("File2 : EOF\n");
        cnt1 = fread(diff_buff1, sizeof(diff_buff1), 1,fp1);
        cnt2 = fread(diff_buff2, sizeof(diff_buff2), 1,fp2);
    }

    free(diff_buff1);
    free(diff_buff2);
}

bool compareBuffers(unsigned char *buff1,unsigned char *buff2, int *pos_err)
{

    int buff1_len = strlen(buff1), buff2_len = strlen(buff2);
    int loop_len = (buff1_len>buff2_len?buff1_len:buff2_len);
    int i =0;
    while(i<loop_len)
    {
        unsigned char c1 = buff1[i];
        unsigned char c2 = buff2[i];
        if(c1 != c2)
        {
            *pos_err = i;
            return false;
        }
        i++;
    }

    return true;
}

void compareFiles(FILE *fp1, FILE *fp2) 
{ 

    unsigned char *buff1, *buff2;

    buff1 = malloc(sizeof(unsigned char)*BUFF_LEN);
    buff2 = malloc(sizeof(unsigned char)*BUFF_LEN);

    int cnt1 = fread(buff1, sizeof(buff1), 1,fp1);
    int cnt2 = fread(buff2, sizeof(buff2), 1,fp2);

    int fp1_pos = 0, fp2_pos = 0;
    int buff_pos_err = 0;
    int buff_cnt = 0;
    // iterate loop till some buffer is read from file
    while (cnt1 || cnt2) 
    { 
        if(cnt1 == 0)
        {
            printf("File1 EOF\n");
            return;
        }

        if(cnt2 == 0)
        {
            printf("File2 EOF\n");
            return;
        }

        // if buffers are different
        if (!compareBuffers(buff1,buff2,&buff_pos_err))
        {
            printf("Files are different at offset = %d\n", (buff_cnt*BUFF_LEN) + buff_pos_err+1);

            print_diff_16_bytes(fp1, fp2, (buff_cnt*BUFF_LEN) + buff_pos_err);

            return;
        }
        memset( buff1, 0, sizeof(unsigned char)*BUFF_LEN );
        memset( buff2, 0, sizeof(unsigned char)*BUFF_LEN );

        cnt1 = fread(buff1, sizeof(unsigned char), BUFF_LEN,fp1);
        cnt2 = fread(buff2, sizeof(unsigned char), BUFF_LEN,fp2);

        buff_cnt++;
    } 

    free(buff1);
    free(buff1);

    printf("Files are identical\n");
} 
  
// Driver code 
int main(int argc, char* argv[]) 
{ 
    if(argc < 3)
    {   
        printf("Insufficient args\nyour args:\n");
        for(int i=0;i<argc;i++)
            printf("%s\n",argv[i] );
        printf("Required 2 filenames");
        return -1;
    }

    clock_t t; 
    t = clock(); 

    FILE *fp1 = fopen(argv[1], "r"); 
    FILE *fp2 = fopen(argv[2], "r"); 
  
    if (fp1 == NULL || fp2 == NULL) 
    { 
       printf("Error : Files could not open"); 
       return -1; 
    } 
  
    t = clock() - t; 
    double time_taken = ((double)t)/(CLOCKS_PER_SEC/1000); // in milli_seconds 

    compareFiles(fp1, fp2); 

    printf("\ncompareFiles() took %.3f ms to execute \n", time_taken); 
  
    // closing both file 
    fclose(fp1); 
    fclose(fp2); 

    return 0; 
} 